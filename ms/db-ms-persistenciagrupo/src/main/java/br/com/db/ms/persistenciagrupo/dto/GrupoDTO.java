package br.com.db.ms.persistenciagrupo.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GrupoDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	public GrupoDTO() {
		
	}
	
	public GrupoDTO(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public String id;
	public String name;
}
