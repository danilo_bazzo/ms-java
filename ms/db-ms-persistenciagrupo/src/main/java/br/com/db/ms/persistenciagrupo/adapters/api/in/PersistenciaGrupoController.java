package br.com.db.ms.persistenciagrupo.adapters.api.in;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.db.ms.persistenciagrupo.core.PersistenciaGrupoService;
import br.com.db.ms.persistenciagrupo.dto.GrupoDTO;

@Controller
@RequestMapping("persistenciaGrupo")
@CrossOrigin(origins = "${crossOrigin.origins}", allowedHeaders = "${crossOrigin.allowedHeaders}")
public class PersistenciaGrupoController {
	
	@Autowired
	private PersistenciaGrupoService service;
	
	@GetMapping(
		produces = { MediaType.APPLICATION_JSON_VALUE }
	)
	public ResponseEntity<Object> get()
	{
		try {
			List<GrupoDTO> list = service.findAll();
			
			if(!list.isEmpty())
				return new ResponseEntity<>(list, HttpStatus.OK);
			else
				return new ResponseEntity<>("Não existe registros", HttpStatus.NO_CONTENT);
		}
		catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping(
			value = "/grupomock",
			produces = { MediaType.APPLICATION_JSON_VALUE }
		)
		public ResponseEntity<Object> getMock()
		{
			try {
				List<GrupoDTO> list = new ArrayList<GrupoDTO>();
				list.add(new GrupoDTO("1","Grupo  1 - Mock"));
				
				if(!list.isEmpty())
					return new ResponseEntity<>(list, HttpStatus.OK);
				else
					return new ResponseEntity<>("Não existe registros", HttpStatus.NO_CONTENT);
			}
			catch (Exception e) {
				return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	
	@GetMapping(
		value = "{id}",
		produces = { MediaType.APPLICATION_JSON_VALUE }
	)
	public ResponseEntity<Object> get(@PathVariable String id)
	{
		try {
			GrupoDTO dto = service.findById(id);
			
			if(dto != null)
				return new ResponseEntity<>(dto, HttpStatus.OK);
			else
				return new ResponseEntity<>("Não existe registros", HttpStatus.NO_CONTENT);
		}
		catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(
		consumes = { MediaType.APPLICATION_JSON_VALUE },
		produces = { MediaType.APPLICATION_JSON_VALUE }
	)
	public ResponseEntity<Object> insert(@RequestBody GrupoDTO dto)
	{
		try {
			dto.id = service.insert(dto);
			
			if(dto.id != null)
				return new ResponseEntity<>(dto, HttpStatus.OK);
			else
				return new ResponseEntity<>("Ocorreu um erro ao inserir o registro", HttpStatus.BAD_REQUEST);
		}
		catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PatchMapping(
		consumes = { MediaType.APPLICATION_JSON_VALUE },
		produces = { MediaType.APPLICATION_JSON_VALUE }
	)
	public ResponseEntity<Object> update(@RequestBody GrupoDTO dto)
	{
		try { 
			if(service.update(dto))
				return new ResponseEntity<>(dto, HttpStatus.OK);
			else
				return new ResponseEntity<>("Não existe registros", HttpStatus.NO_CONTENT);
		}
		catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping(
		value = "{id}",
		produces = { MediaType.APPLICATION_JSON_VALUE }
	)
	public ResponseEntity<Object> delete(@PathVariable String id)
	{
		try {
			GrupoDTO dto = service.delete(id);
			if(dto!=null)
				return new ResponseEntity<>(dto, HttpStatus.OK);
			else
				return new ResponseEntity<>("Não existe registros", HttpStatus.NO_CONTENT);
		}
		catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
