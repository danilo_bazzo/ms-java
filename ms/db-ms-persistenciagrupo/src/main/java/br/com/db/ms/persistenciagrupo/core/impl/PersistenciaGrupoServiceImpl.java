package br.com.db.ms.persistenciagrupo.core.impl;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.db.ms.persistenciagrupo.core.PersistenciaGrupoService;
import br.com.db.ms.persistenciagrupo.dto.GrupoDTO;
import br.com.db.ms.persistenciagrupo.mapper.GrupoMapper;
import br.com.db.ms.persistenciagrupo.modal.GrupoDO;
import br.com.db.ms.persistenciagrupo.repository.PersistenciaGrupoRepository;

@Service
public class PersistenciaGrupoServiceImpl implements PersistenciaGrupoService {
	
	@Autowired
	private PersistenciaGrupoRepository repository;

	public List<GrupoDTO> findAll(){
		
		List<GrupoDO> result = new ArrayList<GrupoDO>();
		Iterable<GrupoDO> iterator = repository.findAll();
		
		iterator.forEach(result::add);
        
		return GrupoMapper.toDTO(result);
	}
	
	public GrupoDTO findById(String id){
		try {
			
			Optional<GrupoDO> model = repository.findById(id);
			
			if(model != null)
				return GrupoMapper.toDTO(model.get());
			else
				return null;
		}
		catch (Exception e) {
			throw e;
		}
	}
	
	public String insert(GrupoDTO dto){
		try {
			return repository.save(GrupoMapper.toDO(dto)).getId();
		}
		catch (Exception e) {
			throw e;
		}
	}
	
	public boolean update(GrupoDTO dto){
		try {
			if(this.findById(dto.id) != null) 
			{
				repository.save(GrupoMapper.toDO(dto));
				return true;
			}
			else
				return false;
		}
		catch (Exception e) {
			throw e;
		}
	}
	
	public GrupoDTO delete(String id){
		try {
			GrupoDTO dto = this.findById(id);
			
			if(dto != null) 
			{
				repository.delete(GrupoMapper.toDO(dto));
				return dto;
			}
			else
				return null;
		}
		catch (Exception e) {
			throw e;
		}
	}
}
