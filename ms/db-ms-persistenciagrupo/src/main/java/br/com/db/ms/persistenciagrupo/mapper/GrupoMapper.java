package br.com.db.ms.persistenciagrupo.mapper;

import java.util.ArrayList;
import java.util.List;

import br.com.db.ms.persistenciagrupo.dto.GrupoDTO;
import br.com.db.ms.persistenciagrupo.modal.GrupoDO;

public class GrupoMapper
{
	public static List<GrupoDTO> toDTO(List<GrupoDO> list){
		
		List<GrupoDTO> result = new ArrayList<GrupoDTO>();
		
		for(GrupoDO modal : list)
			result.add(toDTO(modal));
		
		return result;
	}
	
	public static GrupoDTO toDTO(GrupoDO modal){
		GrupoDTO dto = new GrupoDTO();
		dto.id = modal.getId();
		dto.name = modal.getName();
		return dto;
	}
	
	public static List<GrupoDO> toDO(List<GrupoDTO> list){
		
		List<GrupoDO> result = new ArrayList<GrupoDO>();
		
		for(GrupoDTO dto : list)
			result.add(toDO(dto));
		
		return result;
	}
	
	public static GrupoDO toDO(GrupoDTO dto){
		GrupoDO modal = new GrupoDO();
		modal.setId(dto.id);
		modal.setName(dto.name);
		return modal;
	}
}
