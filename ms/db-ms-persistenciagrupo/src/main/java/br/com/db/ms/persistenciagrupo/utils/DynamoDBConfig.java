package br.com.db.ms.persistenciagrupo.utils;

import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;

@Configuration
@EnableDynamoDBRepositories(basePackages = "br.com.db.ms.persistenciagrupo.repository")
public class DynamoDBConfig {
	
    @Value("${amazon.aws.accesskey}")
    public String accessKey;

    @Value("${amazon.aws.secretkey}")
    public String secretkey;
    
    @Bean
    public AmazonDynamoDB amazonDynamoDB() {
        AmazonDynamoDB amazonDynamoDB = new AmazonDynamoDBClient(new BasicAWSCredentials(accessKey, secretkey));
        
        amazonDynamoDB.setRegion(Region.getRegion(Regions.US_EAST_2));
        
        return amazonDynamoDB;
    }
}
