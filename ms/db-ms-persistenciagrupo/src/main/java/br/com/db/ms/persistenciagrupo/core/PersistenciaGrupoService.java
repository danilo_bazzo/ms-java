package br.com.db.ms.persistenciagrupo.core;

import java.util.List;

import br.com.db.ms.persistenciagrupo.dto.GrupoDTO;

public interface PersistenciaGrupoService {
	
	public List<GrupoDTO> findAll();
	
	public GrupoDTO findById(String id);
	
	public String insert(GrupoDTO dto);
	
	public boolean update(GrupoDTO dto);
	
	public GrupoDTO delete(String id);
}
