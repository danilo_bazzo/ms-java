package br.com.db.ms.persistenciagrupo.repository;


import java.util.Optional;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import br.com.db.ms.persistenciagrupo.modal.GrupoDO;

@EnableScan
public interface PersistenciaGrupoRepository extends 
  CrudRepository<GrupoDO, String> {
    
    Optional<GrupoDO> findById(Integer id);
}