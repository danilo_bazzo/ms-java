package br.com.db.ms.persistenciausuario.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GrupoDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	public String id;
	public String name;
}
