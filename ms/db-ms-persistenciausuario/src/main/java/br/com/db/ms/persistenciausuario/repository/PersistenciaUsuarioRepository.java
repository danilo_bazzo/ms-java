package br.com.db.ms.persistenciausuario.repository;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.AttributeUpdate;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;

import br.com.db.ms.persistenciausuario.modal.UsuarioDO;

@Repository
public class PersistenciaUsuarioRepository
{
    @Value("${amazon.aws.accesskey}")
    public String accessKey;

    @Value("${amazon.aws.secretkey}")
    public String secretkey;
    
	public List<UsuarioDO> findAll()
	{
        DynamoDB dynamoDB = new DynamoDB(new AmazonDynamoDBClient(new BasicAWSCredentials(accessKey, secretkey)).withRegion(Regions.US_EAST_2));
        
        List<UsuarioDO> result = new ArrayList<UsuarioDO>();
        
        Iterator<Item> iterator = dynamoDB.getTable("TB_ET_USUARIO").scan().iterator();
        
        while (iterator.hasNext()) {
            Item item = iterator.next();
            
            UsuarioDO dto = new UsuarioDO();
            dto.id = item.getString("id");
            dto.name = item.getString("name");
            dto.login = item.getString("login");
            dto.email = item.getString("email");
            result.add(dto);
        }
        
		return result;
	}
	
	public UsuarioDO findById(String id)
	{
        DynamoDB dynamoDB = new DynamoDB(new AmazonDynamoDBClient(new BasicAWSCredentials(accessKey, secretkey)).withRegion(Regions.US_EAST_2));
        
        QuerySpec spec = new QuerySpec()
        	    .withKeyConditionExpression("id = :id")
        	    .withValueMap(new ValueMap()
        	        .withString(":id", id));

        Iterator<Item> iterator = dynamoDB.getTable("TB_ET_USUARIO").query(spec).iterator();
        
        if(iterator.hasNext()) {
        	UsuarioDO model = new UsuarioDO();
            Item item = iterator.next();
            model.id = item.getString("id");
            model.name = item.getString("name");
            model.login = item.getString("login");
            model.email = item.getString("email");
            return model;
        }
        else
        	return null;
	}
	
	public String insert(UsuarioDO modal)
	{
		try 
		{
	        DynamoDB dynamoDB = new DynamoDB(new AmazonDynamoDBClient(new BasicAWSCredentials(accessKey, secretkey)).withRegion(Regions.US_EAST_2));
	        
	        modal.id = UUID.randomUUID().toString();
	        
			Item item = new Item()
			  .withPrimaryKey("id", modal.id)
			  .withString("name", modal.name)
			  .withString("login", modal.login)
			  .withString("email", modal.email);
	        
	        dynamoDB.getTable("TB_ET_USUARIO").putItem(item);
	        
			return modal.id;
		}
		catch(Exception e) {
			modal.id = null;
			throw e;
		}
	}
	
	public boolean update(UsuarioDO modal)
	{
		try 
		{
	        DynamoDB dynamoDB = new DynamoDB(new AmazonDynamoDBClient(new BasicAWSCredentials(accessKey, secretkey)).withRegion(Regions.US_EAST_2));
	        
	        Table updateTable = dynamoDB.getTable("TB_ET_USUARIO");
	        PrimaryKey pk = new PrimaryKey("id", modal.id);
	        
	        updateTable.updateItem(pk, new AttributeUpdate("name").put(modal.name));
	        updateTable.updateItem(pk, new AttributeUpdate("email").put(modal.email));
	        
			return true;
		}
		catch(Exception e) {
			throw e;
		}
	}
	
	public boolean delete(UsuarioDO modal)
	{
		try 
		{
	        DynamoDB dynamoDB = new DynamoDB(new AmazonDynamoDBClient(new BasicAWSCredentials(accessKey, secretkey)).withRegion(Regions.US_EAST_2));
	        
	        dynamoDB.getTable("TB_ET_USUARIO").deleteItem(new PrimaryKey("id", modal.id));
	        
			return true;
		}
		catch(Exception e) {
			throw e;
		}
	}
}