package br.com.db.ms.persistenciausuario.adapters.api.out;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import com.amazonaws.services.dynamodbv2.model.AmazonDynamoDBException;

//import br.com.db.ms.persistenciausuario.adapters.messagebrokers.kafka.producer.PersistenciaGrupoProducer;
import br.com.db.ms.persistenciausuario.dto.GrupoDTO;


@Controller
@RequestMapping("persistenciaUsuario")
@CrossOrigin(origins = "${crossOrigin.origins}", allowedHeaders = "${crossOrigin.allowedHeaders}")
public class PersistenciaGrupoController {
	
	@Value("${persistenciaGrupo.endpoint}")
	private String endpoint;
	
	private RestTemplate restTemplate;
	
	//@Autowired
	//private PersistenciaGrupoProducer producer;
	
	public PersistenciaGrupoController() {
		RestTemplateBuilder build = new RestTemplateBuilder();
		restTemplate = build.build();
	}
	
	@GetMapping("/grupos")
	public ResponseEntity<Object> getGrupos()
	{
		try {
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			
			GrupoDTO filtro = new GrupoDTO();
			
			HttpEntity<GrupoDTO> request = new HttpEntity<GrupoDTO>(filtro, headers);
			return restTemplate.exchange(endpoint, HttpMethod.GET, request, Object.class);
			
		}
		catch (AmazonDynamoDBException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@DeleteMapping("/deleteGrupo")
	public void deleteGrupoViaKafka()
	{
		try {
			//producer.send("1");
		}
		catch (Exception e) {
			throw e;
		}
	}
}
