package br.com.db.ms.persistenciausuario.core.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.db.ms.persistenciausuario.core.PersistenciaUsuarioService;
import br.com.db.ms.persistenciausuario.dto.UsuarioDTO;
import br.com.db.ms.persistenciausuario.mapper.UsuarioMapper;
import br.com.db.ms.persistenciausuario.repository.PersistenciaUsuarioRepository;

@Service
public class PersistenciaUsuarioServiceImpl implements PersistenciaUsuarioService {
	
	@Autowired
	private PersistenciaUsuarioRepository repository;

	public List<UsuarioDTO> findAll(){
		return UsuarioMapper.toDTO(repository.findAll());
	}
	
	public UsuarioDTO findById(String id){
		return UsuarioMapper.toDTO(repository.findById(id));
	}
	
	public String insert(UsuarioDTO dto){
		
		return repository.insert(UsuarioMapper.toDO(dto));
	}
	
	public boolean update(UsuarioDTO dto){
		try {
			if(this.findById(dto.id) != null) 
			{
				repository.update(UsuarioMapper.toDO(dto));
				return true;
			}
			else
				return false;
		}
		catch (Exception e) {
			throw e;
		}
	}
	
	public UsuarioDTO delete(String id){
		try {
			UsuarioDTO dto = this.findById(id);
			
			if(dto != null) 
			{
				repository.delete(UsuarioMapper.toDO(dto));
				return dto;
			}
			else
				return null;
		}
		catch (Exception e) {
			throw e;
		}
	}
}
