package br.com.db.ms.persistenciausuario.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	public String id;
	public String login;
	public String name;
	public String email;
	
}
