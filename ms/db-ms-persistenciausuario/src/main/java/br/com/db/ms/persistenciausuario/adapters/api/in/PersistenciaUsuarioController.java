package br.com.db.ms.persistenciausuario.adapters.api.in;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.amazonaws.services.dynamodbv2.model.AmazonDynamoDBException;

import br.com.db.ms.persistenciausuario.core.PersistenciaUsuarioService;
import br.com.db.ms.persistenciausuario.dto.UsuarioDTO;


@Controller
@RequestMapping("persistenciaUsuario")
@CrossOrigin(origins = "${crossOrigin.origins}", allowedHeaders = "${crossOrigin.allowedHeaders}")
public class PersistenciaUsuarioController {
	
	@Autowired
	private PersistenciaUsuarioService service;
	
	@GetMapping()
	public ResponseEntity<Object> get()
	{
		try {
			List<UsuarioDTO> list = service.findAll();
			
			if(!list.isEmpty())
				return new ResponseEntity<>(list, HttpStatus.OK);
			else
				return new ResponseEntity<>("Não existe registros", HttpStatus.NO_CONTENT);
		}
		catch (AmazonDynamoDBException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("{id}")
	public ResponseEntity<Object> get(@PathVariable String id)
	{
		try {
			UsuarioDTO dto = service.findById(id);
			
			if(dto != null)
				return new ResponseEntity<>(dto, HttpStatus.OK);
			else
				return new ResponseEntity<>("Não existe registros", HttpStatus.NO_CONTENT);
		}
		catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping()
	public ResponseEntity<Object> insert(@RequestBody UsuarioDTO dto)
	{
		try 
		{			
			dto.id = service.insert(dto);
			if(dto.id != null)
				return new ResponseEntity<>(dto, HttpStatus.OK);
			else
				return new ResponseEntity<>("Ocorreu um erro ao inserir o usuário", HttpStatus.BAD_REQUEST);
		}
		catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PatchMapping()
	public ResponseEntity<Object> update(@RequestBody UsuarioDTO dto)
	{
		try 
		{			
			if(service.update(dto))
				return new ResponseEntity<>(dto, HttpStatus.OK);
			else
				return new ResponseEntity<>("Ocorreu um erro ao inserir o usuário", HttpStatus.BAD_REQUEST);
		}
		catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<Object> delete(@PathVariable String id)
	{
		try {
			UsuarioDTO dto = service.delete(id);
			if(dto!=null)
				return new ResponseEntity<>(dto, HttpStatus.OK);
			else
				return new ResponseEntity<>("Não existe registros", HttpStatus.NO_CONTENT);
		}
		catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
