package br.com.db.ms.persistenciausuario.modal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioDO
{
	public String id;
	public String login;
	public String name;
	public String email;
}
