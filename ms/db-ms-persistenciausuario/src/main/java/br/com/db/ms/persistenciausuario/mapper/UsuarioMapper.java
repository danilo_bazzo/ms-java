package br.com.db.ms.persistenciausuario.mapper;

import java.util.ArrayList;
import java.util.List;

import br.com.db.ms.persistenciausuario.dto.UsuarioDTO;
import br.com.db.ms.persistenciausuario.modal.UsuarioDO;

public class UsuarioMapper
{	
	public static List<UsuarioDTO> toDTO(List<UsuarioDO> list){
		
		List<UsuarioDTO> result = new ArrayList<UsuarioDTO>();
		
		for(UsuarioDO modal : list)
			result.add(toDTO(modal));
		
		return result;
	}
	
	public static  UsuarioDTO toDTO(UsuarioDO modal){
		UsuarioDTO dto = new UsuarioDTO();
		dto.id = modal.id;
		dto.name = modal.name;
		dto.login = modal.login;
		dto.email = modal.email;
		return dto;
	}
	
	public static List<UsuarioDO> toDO(List<UsuarioDTO> list){
		
		List<UsuarioDO> result = new ArrayList<UsuarioDO>();
		
		for(UsuarioDTO dto : list)
			result.add(toDO(dto));
		
		return result;
	}
	
	public static  UsuarioDO toDO(UsuarioDTO dto){
		UsuarioDO modal = new UsuarioDO();
		modal.id = dto.id;
		modal.name = dto.name;
		modal.login = dto.login;
		modal.email = dto.email;
		return modal;
	}
	
	
}
