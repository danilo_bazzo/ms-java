package br.com.db.ms.persistenciausuario.core;

import java.util.List;

import br.com.db.ms.persistenciausuario.dto.UsuarioDTO;

public interface PersistenciaUsuarioService {
	
	public List<UsuarioDTO> findAll();
	
	public UsuarioDTO findById(String id);
	
	public String insert(UsuarioDTO dto);
	
	public boolean update(UsuarioDTO dto);
	
	public UsuarioDTO delete(String id);
}
