import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams, HttpClient, HttpHeaders} from '@angular/common/http';
import { PersistenciaUsuarioModel } from '../components/models/persistencia-usuario.model';

@Injectable({providedIn:'root'})
export class PersistenciaUsuarioService {
    
    baseUrl = 'http://db-ms-persistenciausuario.us-east-2.elasticbeanstalk.com/persistenciaUsuario';
    //baseUrl = 'http://localhost:5000/persistenciaUsuario';

    constructor(private http: HttpClient) { }

    public find(): Observable<PersistenciaUsuarioModel[]>
    {
        const headers = new HttpHeaders();
        headers.append('Access-Control-Allow-Headers', 'Content-Type')
                .append('Access-Control-Allow-Methods', 'GET')
                .append('Access-Control-Allow-Origin', '*');

        return this.http.get<PersistenciaUsuarioModel[]>(this.baseUrl+'/', {headers: headers});
    }
}