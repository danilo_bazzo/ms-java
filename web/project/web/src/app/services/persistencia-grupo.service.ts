import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams, HttpClient, HttpHeaders} from '@angular/common/http';
import { PersistenciaGrupoModel } from '../components/models/persistencia-grupo.model';

@Injectable({providedIn:'root'})
export class PersistenciaGrupoService {
    
    baseUrl = 'http://db-ms-persistenciagrupo.us-east-2.elasticbeanstalk.com/persistenciaGrupo';
    //baseUrl = 'http://localhost:5000/persistenciaGrupo';

    constructor(private http: HttpClient) { }

    public find(): Observable<PersistenciaGrupoModel[]>
    {
        const headers = new HttpHeaders();
        headers.append('Access-Control-Allow-Headers', 'Content-Type')
                .append('Access-Control-Allow-Methods', 'GET')
                .append('Access-Control-Allow-Origin', '*');

        return this.http.get<PersistenciaGrupoModel[]>(this.baseUrl+'/', {headers: headers});
    }
    
    private getParameters(model: PersistenciaGrupoModel){
        let paramsHTTP = new HttpParams();
        paramsHTTP = paramsHTTP.set('id', model.id);
        return paramsHTTP;
    }
}