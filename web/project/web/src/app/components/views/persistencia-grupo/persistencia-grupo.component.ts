import { Component, OnInit } from '@angular/core';
import { PersistenciaGrupoService } from 'src/app/services/persistencia-grupo.service';
import { PersistenciaGrupoModel } from '../../models/persistencia-grupo.model';

@Component({
  selector: 'app-persistencia-grupo',
  templateUrl: './persistencia-grupo.component.html',
  styleUrls: ['./persistencia-grupo.component.css']
})
export class PersistenciaGrupoComponent implements OnInit {

  constructor(private service: PersistenciaGrupoService) { }

  dataSource: PersistenciaGrupoModel[]=[];

  ngOnInit(): void {
    this.loadData();
  }

  public loadData():void{
    this.dataSource = [];
    this.service.find().subscribe((result: PersistenciaGrupoModel[]) => {
      this.dataSource = result;
    }, erro => {
      this.dataSource = [];
    });
  }

}
