import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersistenciaGrupoComponent } from './persistencia-grupo.component';

describe('PersistenciaGrupoComponent', () => {
  let component: PersistenciaGrupoComponent;
  let fixture: ComponentFixture<PersistenciaGrupoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersistenciaGrupoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersistenciaGrupoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
