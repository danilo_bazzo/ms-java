import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersistenciaUsuarioComponent } from './persistencia-usuario.component';

describe('PersistenciaUsuarioComponent', () => {
  let component: PersistenciaUsuarioComponent;
  let fixture: ComponentFixture<PersistenciaUsuarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersistenciaUsuarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersistenciaUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
