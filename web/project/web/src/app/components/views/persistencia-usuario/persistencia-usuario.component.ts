import { Component, OnInit } from '@angular/core';
import { PersistenciaUsuarioService } from 'src/app/services/persistencia-usuario.service';
import { PersistenciaUsuarioModel } from '../../models/persistencia-usuario.model';

@Component({
  selector: 'app-persistencia-usuario',
  templateUrl: './persistencia-usuario.component.html',
  styleUrls: ['./persistencia-usuario.component.css']
})
export class PersistenciaUsuarioComponent implements OnInit {

  constructor(private service: PersistenciaUsuarioService) { }

  dataSource: PersistenciaUsuarioModel[]=[];

  ngOnInit(): void {
    this.loadData();
  }

  public loadData():void{
    this.dataSource = [];
    this.service.find().subscribe((result: PersistenciaUsuarioModel[]) => {
      this.dataSource = result;
    }, erro => {
      this.dataSource = [];
    });
  }

}
